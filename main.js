/*
 * Title:       Character Trait Generator (Charagen)
 * Author:      ablissful
 * Version:     0.0.0
 * Modified:    11 June 2024
 * Description: Generates a list of traits for a character.
 * 
 * Copyright (C) 2024 ablissful
`*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

window.onload = allGen;

// Outputs
let occ_text = document.getElementById("occ-op");
let per_text = document.getElementById("per-op");
let age_text = document.getElementById("age-op");
let clo_text = document.getElementById("clo-op");
let hob_text = document.getElementById("hob-op");
let bod_text = document.getElementById("bod-op");

// Arrays
const jobs = ["pirate", "fireman", "shoemaker", "programmer", "artist"];
const personalities = ["lazy", "serious", "crazy", "wacky", "weird"];
const ages = ["teenager", "young adult", "adult", "elder", "undead"];
const cloths = ["dress", "cardboard box", "overalls", "trenchcoat", "nude"];
const hobbies = ["knitting", "painting", "hiking", "biking", "collecting"];
const bodytypes = ["pear", "hourglass", "rectangle", "triangle", "circle"];

// Buttons
document.getElementById("occ-btn").onclick = jobGen;
document.getElementById("per-btn").onclick = personalityGen;
document.getElementById("age-btn").onclick = ageGen;
document.getElementById("clo-btn").onclick = clothingGen;
document.getElementById("hob-btn").onclick = hobbyGen;
document.getElementById("bod-btn").onclick = bodytypeGen;
document.getElementById("tot-btn").onclick = allGen;

/**
 * Generates a random job trait.
 */
function jobGen() {
  let gen = jobs[Math.floor(Math.random() * 5)];
  while (occ_text.textContent === gen) {
    if (!(occ_text.textContent === gen)) {
      break;
    }
    gen = jobs[Math.floor(Math.random() * jobs.length)];
  }
  occ_text.textContent = gen;
}

/**
 * Generates a random personality trait.
 */
function personalityGen() {
  let gen = personalities[Math.floor(Math.random() * 5)];
  while (per_text.textContent === gen) {
    if (!(per_text.textContent === gen)) {
      break;
    }
    gen = personalities[Math.floor(Math.random() * personalities.length)];
  }
  per_text.textContent = gen;
}

/**
 * Generates a random age trait.
 */
function ageGen() {
  let gen = ages[Math.floor(Math.random() * 5)];
  while (age_text.textContent === gen) {
    if (!(age_text.textContent === gen)) {
      break;
    }
    gen = ages[Math.floor(Math.random() * ages.length)];
  }
  age_text.textContent = gen;
}

/**
 * Generates random clothing.
 */
function clothingGen() {
  let gen = cloths[Math.floor(Math.random() * 5)];
  while (clo_text.textContent === gen) {
    if (!(clo_text.textContent === gen)) {
      break;
    }
    gen = cloths[Math.floor(Math.random() * cloths.length)];
  }
  clo_text.textContent = gen;
}

/**
 * Generates a random hobby.
 */
function hobbyGen() {
  let gen = hobbies[Math.floor(Math.random() * 5)];
  while (hob_text.textContent === gen) {
    if (!(hob_text.textContent === gen)) {
      break;
    }
    gen = hobbies[Math.floor(Math.random() * hobbies.length)];
  }
  hob_text.textContent = gen;
}

/**
 * Generates a random body type.
 */
function bodytypeGen() {
  let gen = bodytypes[Math.floor(Math.random() * 5)];
  while (bod_text.textContent === gen) {
    if (!(bod_text.textContent === gen)) {
      break;
    }
    gen = bodytypes[Math.floor(Math.random() * bodytypes.length)];
  }
  bod_text.textContent = gen;
}

/**
 * Generates all of the traits.
 */
function allGen() {
  jobGen();
  personalityGen();
  ageGen();
  clothingGen();
  hobbyGen();
  bodytypeGen();
}
